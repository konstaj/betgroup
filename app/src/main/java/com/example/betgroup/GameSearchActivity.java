package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;
import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED;

/**
 * This class creates the Game searching activity, which shows available Competitions and allows to create a group.
 */


public class GameSearchActivity extends AppCompatActivity {

    JSONArray jsonarray = new JSONArray();

    ArrayList<Competition> competitionsArray = new ArrayList<>();
    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ListView compList;
    String selectedCompString,selectedCompCode, groupId;
    TextView confirm_add, confirm_yes, confirm_no;
    private CompetitionAdapter adapter;

    private RequestQueue mQueue;
    private ValueEventListener mListener;
    final DatabaseReference compRef = database.getReference("Competitions");
    final DatabaseReference userRef = database.getReference("Users");

    /**
     * Empty main constructor.
     */
    public GameSearchActivity() {
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    startActivity(new Intent(GameSearchActivity.this,ProfileActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                    case R.id.navigation_main:
                    startActivity(new Intent(GameSearchActivity.this,MainActivity.class));
                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        return true;
                case R.id.navigation_groups:
                    startActivity(new Intent(GameSearchActivity.this,MyGroupsActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gamesearch);
        compList = findViewById(R.id.competition_list);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.getMenu().getItem(2).setChecked(true);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        loadCompetitions();



        compList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Competition selectedComp= competitionsArray.get(position);
                confirm_add = findViewById(R.id.confirm_adding);
                confirm_no = findViewById(R.id.addno);
                confirm_yes = findViewById(R.id.addyes);
                selectedCompString = selectedComp.getName();
                selectedCompCode = selectedComp.getCode();
                confirm_add.setText(getString(R.string.CreateGroupFor) + "\n" + selectedCompString);
                confirm_add.setVisibility(View.VISIBLE);
                confirm_no.setVisibility(View.VISIBLE);
                confirm_yes.setVisibility(View.VISIBLE);


            }
        });
    }

    /**
     * Get competition data from football-data-api using Volley
     */
    public void loadCompetitions(){

        // Initialize a new RequestQueue instance
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                "https://api.football-data.org/v2/competitions/",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Process the JSON
                        try{
                            JSONArray jsonArray = response.getJSONArray("competitions");
                            if (jsonArray != null) {
                                int len = jsonArray.length();
                                for (int i=0;i<len;i++){
                                    String startday;
                                    JSONObject competition = jsonArray.getJSONObject(i);
                                    String name = competition.getString("name");
                                    String area = competition.getJSONObject("area").getString("name");
                                    try{
                                    startday = competition.getJSONObject("currentSeason").getString("startDate");
                                    }
                                    catch(Exception e){
                                        startday = "";
                                    }
                                    String code = competition.getString("code");
                                    //filtering plans that are free
                                    String plan = competition.getString("plan");
                                    //filter unusable competitions
                                    if(plan.equals("TIER_ONE") ){
                                        competitionsArray.add(new Competition(name, area, code, startday));
                                    }

                                }
                                adapter = new CompetitionAdapter(GameSearchActivity.this, competitionsArray);
                                compList.setAdapter(adapter);
                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){

                    }
                }
        ){
            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                headers.put("X-Auth-Token", "1e583c3933cb49f68131902708844759");
                return headers;
            }

        };

        // Add JsonObjectRequest to the RequestQueue
        requestQueue.add(jsonObjectRequest);




    }

    /**
     * Onclick method on confirming the group and sending the data to Firebase
     * @param view
     */
        public void confirmNewGroup(View view){
        confirm_add.setVisibility(View.GONE);
        confirm_no.setVisibility(View.GONE);
        confirm_yes.setVisibility(View.GONE);
        //initialize group db structure
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String databasename = dataSnapshot.child(auth.getUid()).child("name").getValue().toString();

                userRef.child(auth.getUid()).child("groups").push();
                groupId = userRef.push().getKey();
                userRef.child(auth.getUid()).child("groups").child(groupId).setValue("owner");
                compRef.child(groupId).child("owner").child("id").setValue(auth.getUid());
                compRef.child(groupId).child("owner").child("name").setValue(databasename);
                compRef.child(groupId).child("rules").child("1X2").setValue(3);
                compRef.child(groupId).child("rules").child("totalscore").setValue(1);
                compRef.child(groupId).child("rules").child("teamscore").setValue(1);
                compRef.child(groupId).child("code").setValue(selectedCompCode);
                compRef.child(groupId).child("name").setValue(selectedCompCode + " - " + groupId);
                compRef.child(groupId).child("users").child(auth.getUid()).child("name").setValue(databasename);

                Intent intent = new Intent(GameSearchActivity.this, MyGroupsActivity.class);
                intent.putExtra("groupid", groupId);
                intent.putExtra("competition", selectedCompString);
                intent.putExtra("competitioncode", selectedCompCode);
                intent.putExtra("new", "new");
                startActivity(intent);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });


    }

    /**
     * Hide elements
     * @param view
     */
    public void hideConfirm(View view) {
        confirm_add.setVisibility(View.GONE);
        confirm_no.setVisibility(View.GONE);
        confirm_yes.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}



