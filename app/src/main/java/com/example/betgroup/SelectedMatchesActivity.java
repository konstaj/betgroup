package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;

/**
 * Activity class to edit scheduled games and review ended games and their scores.
 */
public class SelectedMatchesActivity  extends AppCompatActivity {
    private String groupId;
    private MatchEditAdapter scheduledadapter,finishedadapter;
    private ArrayList<Match> matches = new ArrayList<>();
    private ArrayList<Match> finishedmatches = new ArrayList<>();
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference groupRef = database.getReference("Competitions");
    private ValueEventListener groupListener;
    Match selectedMatch;
    ListView matchesListview,finishedListView;
    ConstraintLayout modalLayout;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_back:
                    startActivity(new Intent(SelectedMatchesActivity.this,MyGroupsActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    finish();
                    return true;
                case R.id.navigation_stats:
                    Intent intent2 = new Intent(SelectedMatchesActivity.this,SelectedGroupActivity.class);
                    intent2.putExtra("groupname",getIntent().getStringExtra("groupname"));
                    intent2.putExtra("groupid",getIntent().getStringExtra("groupid"));
                    startActivity(intent2);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                case R.id.navigation_info:
                    Intent intent3 = new Intent(SelectedMatchesActivity.this,SelectedInfoActivity.class);
                    intent3.putExtra("groupname",getIntent().getStringExtra("groupname"));
                    intent3.putExtra("groupid",getIntent().getStringExtra("groupid"));
                    startActivity(intent3);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectedmatches);

        TextView moduletitle = findViewById(R.id.moduletext_matches);
        BottomNavigationView navigation = findViewById(R.id.navigation2);
        navigation.getMenu().getItem(2).setChecked(true);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        matchesListview = findViewById(R.id.scheduledmatches);
        finishedListView = findViewById(R.id.finishedmatches);
        moduletitle.setText(getIntent().getStringExtra("groupname"));
        groupId = getIntent().getStringExtra("groupid");


        scheduledadapter = new MatchEditAdapter(SelectedMatchesActivity.this, matches, R.layout.editmatchesrow);
        finishedadapter = new MatchEditAdapter(SelectedMatchesActivity.this, finishedmatches, R.layout.finsihedmatchrow);
        loadScheduledMatches();
                /**
                 * Init Enter key listeners for editing scores
                 */
                matchesListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                      selectedMatch = matches.get(position);
                      final EditText editaway = findViewById(R.id.awayEdit);
                      final EditText edithome = findViewById(R.id.homeEdit);
                      final TextView editmodal = findViewById(R.id.setguessmodal);
                      final Button acceptbtn = findViewById(R.id.savebet);
                      modalLayout = findViewById(R.id.modallayout);
                      modalLayout.setVisibility(View.VISIBLE);
                      modalLayout.bringToFront();
                      editaway.setVisibility(View.VISIBLE);
                      edithome.setVisibility(View.VISIBLE);
                      editmodal.setVisibility(View.VISIBLE);
                      acceptbtn.setVisibility(View.VISIBLE);
                      editmodal.setText(selectedMatch.getHome() + " - " + selectedMatch.getAway());
                      editaway.setText(Integer.toString(selectedMatch.getAwayscore()));
                      edithome.setText(Integer.toString(selectedMatch.getHomescore()));


                    }

                });


    }
    public void loadScheduledMatches(){
        groupListener = groupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                finishedadapter.clear();
                scheduledadapter.clear();
                try{


                    for(DataSnapshot uniqueKeySnapshot : dataSnapshot.child(groupId).child("users").child(auth.getUid()).child("matches").getChildren()){

                        String matchId = uniqueKeySnapshot.getKey();
                        String homename = uniqueKeySnapshot.child("home").getValue().toString();
                        String awayname = uniqueKeySnapshot.child("away").getValue().toString();
                        Integer homescore = Integer.parseInt(uniqueKeySnapshot.child("homescore").getValue().toString());
                        Integer awayscore = Integer.parseInt(uniqueKeySnapshot.child("awayscore").getValue().toString());
                        String startTime = uniqueKeySnapshot.child("startTime").getValue().toString();
                        String status = uniqueKeySnapshot.child("status").getValue().toString();


                        if(status.equals("SCHEDULED")) {
                            matches.add(new Match(matchId,homename,awayname,homescore,awayscore,startTime));
                        }if(status.equals("FINISHED")) {
                            finishedmatches.add(new Match(matchId,homename,awayname,homescore,awayscore,startTime));
                        }
                    }

                } catch (Exception e) {
                }

                matchesListview.setAdapter(scheduledadapter);
                finishedListView.setAdapter(finishedadapter);
            }
            @Override
            public void onCancelled (DatabaseError databaseError){


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        matchesListview.setAdapter(scheduledadapter);
        finishedListView.setAdapter(finishedadapter);
    }

    public void saveBet(View view){
        final EditText editaway = findViewById(R.id.awayEdit);
        final EditText edithome = findViewById(R.id.homeEdit);
        modalLayout = findViewById(R.id.modallayout);
        Integer home = Integer.parseInt(edithome.getText().toString());
        Integer away = Integer.parseInt(editaway.getText().toString());
        groupRef.child(groupId).child("users").child(auth.getUid()).child("matches").child(selectedMatch.getId()).child("homescore").setValue(home);
        groupRef.child(groupId).child("users").child(auth.getUid()).child("matches").child(selectedMatch.getId()).child("awayscore").setValue(away);
        modalLayout.setVisibility(View.GONE);
        Toast.makeText(SelectedMatchesActivity.this, "saved", Toast.LENGTH_SHORT).show();

    }

}

