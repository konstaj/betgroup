package com.example.betgroup;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;


/**
 * Created by kona on 25.2.2019.
 */

@IgnoreExtraProperties
public class User {
    public String name;
    public String email;
    public String id;
    public String score;
    private DatabaseReference mDatabase;

    User(){

    }

    /**
     * Constructor for Userdata that is created on first login.
     * @param id
     * @param name
     * @param email
     * @param score
     */
    public User(String id ,String name, String email, String score) {
        this.name = name;
        this.email = email;
        this.id = id;
        this.score = score;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    /**
     * Write the users data to the database*
     * @param userId
     * @param name
     * @param email
     */
    public void writeNewUser(String userId, String name, String email) {
        User user = new User(userId, name, email,"groups");

        mDatabase = FirebaseDatabase.getInstance().getReference("Users");

        mDatabase.child(userId).setValue(user);
    }
}


