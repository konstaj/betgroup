package com.example.betgroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Datamodel for competition
 */
public class Competition {
String name, area,code, startDate;


    public Competition(String name, String area, String code, String startDate) {
        this.name = name;
        this.area = area;
        this.code = code;
        this.startDate = startDate;
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

}
