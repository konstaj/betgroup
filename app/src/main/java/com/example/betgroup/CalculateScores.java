package com.example.betgroup;

/**
 * Class to do the math for the final scores
 */
public class CalculateScores {

    public int calculateWinner(int home, int away, int rule, int realHome, int realAway){
        int score = 0;
        int realScore = 0;
        if(home == -1){
            return 0;
        }
        if((home - away) == 0){
            score = 1;
        }
        if((home - away) > 0){
            score = 2;
        }
        if((home - away) < 0){
            score = 3;
        }
        if(realHome- realAway == 0){
            realScore = 1;
        }
        if(realHome - realAway > 0){
            realScore = 2;
        }
        if(realHome - realAway < 0){
            realScore = 3;
        }
        if(realScore==score && score > 0) {
            return rule;
        }else{
            return 0;
        }
    }
    public int calculateTotal(int home, int away, int rule, int realHome, int realAway){
        if(home == realHome && away == realAway){
            return rule;
        }
        else{
            return 0;

        }

    } public int calculateTeam(int home, int away, int rule, int realHome, int realAway) {
        int returnscore = 0;

        if (home == realHome) {
            returnscore = returnscore + rule;
        }
        else if(away == realAway){
            returnscore = returnscore + rule;

        }
        return returnscore;
    }

}
