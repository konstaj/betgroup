package com.example.betgroup;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter to show invitable people in Listview
 */
public class InviteAdapter extends ArrayAdapter<DbUser> implements View.OnClickListener{

    Context mContext;
    private List<DbUser> users;

    public InviteAdapter( Context context,  List<DbUser> list) {
        super(context, 0 , list);
        mContext = context;
        users = list;
    }

    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.inviterow,parent,false);

        DbUser user = users.get(position);

        TextView name =  listItem.findViewById(R.id.invitelist_name);
        name.setText( user.getName());

        return listItem;
    }

    @Override
    public void onClick(View v) {

    }

}