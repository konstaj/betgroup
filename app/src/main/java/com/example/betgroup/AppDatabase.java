package com.example.betgroup;

import android.arch.persistence.room.RoomDatabase;

/**
 *
 abstract class for room DB
  */
@android.arch.persistence.room.Database(entities = {DbUser.class}, version = 1,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DaoIF userDao();
}