package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Fragment class to show details of your selected group
 */
public class MyGroupsDetailFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mygroup_detailfragment, container);

        if(v!=null) {
            TextView textView =  v.findViewById(R.id.text_rules);
            SpannableString content = new SpannableString(getString(R.string.rules));
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            textView.setText(content);

        }
        return v;
    }


}

