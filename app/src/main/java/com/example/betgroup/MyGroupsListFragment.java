package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.view.View.VISIBLE;


/**
 * Class show your groups in list
 */
public class MyGroupsListFragment extends Fragment {
    private GroupAdapter adapter;
    Boolean isnew = false;
    ArrayList<Group> groups = new ArrayList<>();
    String selectedCompCode, groupId, newgroupId,compName,groupOwnerId,groupOwnerName,groupName;
    String newGroup = "notnew";
    Integer groupWinner,groupTotalscore,groupTeamscore;
    int groupUserCount;
    ListView groupList;

    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference groupRef = database.getReference("Competitions");
    ValueEventListener mListener;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mygroup_listfragment, container);


        final FragmentActivity fActivity = (FragmentActivity) getActivity();
                groupList = v.findViewById(R.id.groups_list);
                Intent intent = getActivity().getIntent();
                newGroup = intent.getStringExtra("new");
                newgroupId = intent.getStringExtra("groupid");
                try{
                    if(newGroup.equals("new")){
                        selectedCompCode = intent.getStringExtra("competitioncode");
                        loadMatchesForGroup(newgroupId);
                    }

                }catch(Exception e){

                }
                groupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Group selectedGroup= groups.get(position);


                            loadMatchesForGroup(selectedGroup.getId());

                        Intent intent = new Intent(getContext(), com.example.betgroup.MyGroupsActivity.class);
                        intent.putExtra("groupname", selectedGroup.getName());
                        intent.putExtra("groupid", selectedGroup.getId());
                        FragmentManager fragmentManager = fActivity.getSupportFragmentManager();

                        // Get right Fragment object.
                        android.support.v4.app.Fragment detailFragment = fragmentManager.findFragmentById(R.id.fragmentDetail);

                        // Get the TextView object in right Fragment.
                        final TextView nametext = detailFragment.getView().findViewById(R.id.detailname);
                        final TextView winnertext = detailFragment.getView().findViewById(R.id.detail_winner);
                        final TextView totalscoretext = detailFragment.getView().findViewById(R.id.detail_totalscore);
                        final TextView teamscoretext = detailFragment.getView().findViewById(R.id.detail_teamscore);
                        final TextView teamsruletext = detailFragment.getView().findViewById(R.id.rulewin3);
                        final TextView totalruletext = detailFragment.getView().findViewById(R.id.rulewin2);
                        final TextView winnerruletext = detailFragment.getView().findViewById(R.id.rulewin);


                        final ImageView arrow = detailFragment.getView().findViewById(R.id.showgroup);
                        MyGroupsActivity activity = (MyGroupsActivity) getActivity();
                        activity.setGroupId(selectedGroup.getId());

                        // Set text in right Fragment TextView.
                        nametext.setText(selectedGroup.getName());

                        winnertext.setVisibility(VISIBLE);
                        teamscoretext.setVisibility(VISIBLE);
                        totalscoretext.setVisibility(VISIBLE);
                        arrow.setVisibility(VISIBLE);
                        teamsruletext.setVisibility(VISIBLE);
                        winnerruletext.setVisibility(VISIBLE);
                        totalruletext.setVisibility(VISIBLE);

                        winnertext.setText(selectedGroup.getWinner().toString());
                        teamscoretext.setText(selectedGroup.getTeamscore().toString());
                        totalscoretext.setText(selectedGroup.getTotalscore().toString());
                        winnertext.setText(selectedGroup.getWinner().toString());
                        teamscoretext.setText(selectedGroup.getTeamscore().toString());
                        totalscoretext.setText(selectedGroup.getTotalscore().toString());


                    }
                });
                adapter = new GroupAdapter(getContext(), groups);
                    mListener = groupRef.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            adapter.clear();

                            try {
                                for (DataSnapshot uniqueKeySnapshot : dataSnapshot.getChildren()) {

                                    //Loop 1 to go through all the child nodes of groups


                                    groupId = uniqueKeySnapshot.getKey();
                                    selectedCompCode = uniqueKeySnapshot.child("code").getValue().toString();
                                    groupOwnerId = uniqueKeySnapshot.child("owner").child("id").getValue().toString();
                                    groupOwnerName = uniqueKeySnapshot.child("owner").child("name").getValue().toString();
                                    groupName = uniqueKeySnapshot.child("name").getValue().toString();
                                    groupUserCount = (int) uniqueKeySnapshot.child("users").getChildrenCount();
                                    groupTeamscore = uniqueKeySnapshot.child("rules").child("teamscore").getValue(Integer.class);
                                    groupTotalscore = uniqueKeySnapshot.child("rules").child("totalscore").getValue(Integer.class);
                                    groupWinner =  uniqueKeySnapshot.child("rules").child("1X2").getValue(Integer.class);

                                    for (DataSnapshot secondKeySnapshot : uniqueKeySnapshot.child("users").getChildren()) {

                                        if (secondKeySnapshot.getKey().equals(auth.getUid())) {
                                            groups.add(new Group(groupName, groupId, groupOwnerId, groupOwnerName, groupUserCount, groupWinner, groupTotalscore, groupTeamscore));
                                        }
                                    }


                                }
                                groupList.setAdapter(adapter);


                            }catch(Exception e){

                            }
                        }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        return v;
            }


    /**
     * Load matches for selected group from API
     * @param selectedId
     */
    public void loadMatchesForGroup(final String selectedId) {
                // Initialize a new RequestQueue instance
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());

                // Initialize a new JsonObjectRequest instance
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                        Request.Method.GET,
                        "https://api.football-data.org/v2/competitions/" + selectedCompCode + "/matches?dateFrom=2019-02-01&dateTo=2019-12-01",
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Do something with response
                                //mTextView.setText(response.toString());
                                // Process the JSON
                                try {
                                    JSONArray jsonArray = response.getJSONArray("matches");
                                    if (jsonArray != null) {
                                        int len = jsonArray.length();
                                        for (int i = 0; i < len-1; i++) {
                                            String startTime,status;
                                            int homescore, awayscore;
                                            JSONObject matches = jsonArray.getJSONObject(i);
                                            String id = matches.getString("id");
                                            String homename = matches.getJSONObject("homeTeam").getString("name");
                                            try {
                                                homescore = matches.getJSONObject("score").getJSONObject("fullTime").getInt("homeTeam");
                                                awayscore = matches.getJSONObject("score").getJSONObject("fullTime").getInt("awayTeam");
                                            }
                                            catch(Exception e){
                                                homescore = 0;
                                                awayscore = 0;
                                            }
                                            String awayname = matches.getJSONObject("awayTeam").getString("name");

                                            startTime = matches.getString("utcDate");
                                            status = matches.getString("status");
                                            groupRef.child(selectedId).child("matches").child(id).child("home").setValue(homename);
                                            groupRef.child(selectedId).child("matches").child(id).child("away").setValue(awayname);
                                            groupRef.child(selectedId).child("matches").child(id).child("homescore").setValue(homescore);
                                            groupRef.child(selectedId).child("matches").child(id).child("awayscore").setValue(awayscore);
                                            groupRef.child(selectedId).child("matches").child(id).child("startTime").setValue(startTime);
                                            groupRef.child(selectedId).child("matches").child(id).child("status").setValue(status);
                                            try {
                                                if (newGroup.equals("new") && !status.equals("FINISHED")) {
                                                    groupRef.child(selectedId).child("users").child(auth.getUid()).child("matches").child(id).child("home").setValue(homename);
                                                    groupRef.child(selectedId).child("users").child(auth.getUid()).child("matches").child(id).child("away").setValue(awayname);
                                                    groupRef.child(selectedId).child("users").child(auth.getUid()).child("matches").child(id).child("homescore").setValue(homescore);
                                                    groupRef.child(selectedId).child("users").child(auth.getUid()).child("matches").child(id).child("awayscore").setValue(awayscore);
                                                    groupRef.child(selectedId).child("users").child(auth.getUid()).child("matches").child(id).child("startTime").setValue(startTime);
                                                    groupRef.child(selectedId).child("users").child(auth.getUid()).child("matches").child(id).child("status").setValue(status);
                                                }
                                            }catch (Exception e){

                                            }
                                        }

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                ) {
                    /**
                     * Passing some request headers
                     */
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        //headers.put("Content-Type", "application/json");
                        headers.put("X-Auth-Token", "1e583c3933cb49f68131902708844759");
                        return headers;
                    }

                };

                // Add JsonObjectRequest to the RequestQueue
                requestQueue.add(jsonObjectRequest);


            }








}

