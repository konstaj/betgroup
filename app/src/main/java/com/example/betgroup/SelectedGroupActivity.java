package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;

/**
 * Activity class to show scores of each groupmember
 */
public class SelectedGroupActivity extends AppCompatActivity {

    User selectedUser;
    String groupId,userName,userId;
    CalculateScores calc ;
    ArrayList<User> users = new ArrayList<>();
    FirebaseAuth auth = FirebaseAuth.getInstance();
    ScoreBoardAdapter adapter;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference userRef = database.getReference("Users");
    DatabaseReference groupRef = database.getReference("Competitions");
    RecyclerView rvUsers;
    Integer realHomeScore,realAwayScore,homeScore,awayScore,winnerRule,totalRule,teamRule,userScore;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_back:
                    startActivity(new Intent(SelectedGroupActivity.this,MyGroupsActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    finish();
                    return true;
                case R.id.navigation_matches:
                    Intent intent2 = new Intent(SelectedGroupActivity.this,SelectedMatchesActivity.class);
                    intent2.putExtra("groupname",getIntent().getStringExtra("groupname"));
                    intent2.putExtra("groupid",getIntent().getStringExtra("groupid"));
                    startActivity(intent2);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
                case R.id.navigation_info:
                    Intent intent3 = new Intent(SelectedGroupActivity.this,SelectedInfoActivity.class);
                    intent3.putExtra("groupname",getIntent().getStringExtra("groupname"));
                    intent3.putExtra("groupid",getIntent().getStringExtra("groupid"));
                    startActivity(intent3);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectedgroup);

        TextView moduletitle = findViewById(R.id.moduletext_selected);
        BottomNavigationView navigation = findViewById(R.id.navigation2);
        navigation.getMenu().getItem(1).setChecked(true);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        moduletitle.setText(getIntent().getStringExtra("groupname"));
        groupId =  getIntent().getStringExtra("groupid");
        rvUsers = findViewById(R.id.sbrecycler);

        loadScoreBoard();
        // Initialize contacts
        // Create adapter passing in the sample user data

        adapter = new ScoreBoardAdapter(getApplicationContext(), users);
        // Attach the adapter to the recyclerview to populate items
        // Set layout manager to position the items
        rvUsers.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public void loadScoreBoard(){

        groupRef.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //get the group rules
                winnerRule = Integer.parseInt(dataSnapshot.child(groupId).child("rules").child("1X2").getValue().toString());
                teamRule = Integer.parseInt(dataSnapshot.child(groupId).child("rules").child("teamscore").getValue().toString());
                totalRule = Integer.parseInt(dataSnapshot.child(groupId).child("rules").child("totalscore").getValue().toString());
                //Loop through actual match scores
                //Loop through users of the group
                for (DataSnapshot usersKeySnapshot : dataSnapshot.child(groupId).child("users").getChildren()) {
                    userId = usersKeySnapshot.getKey();
                    userName = usersKeySnapshot.child("name").getValue().toString();
                    userScore = 0;
                    for(DataSnapshot uniqueKeySnapshot : dataSnapshot.child(groupId).child("matches").getChildren()) {
                        String status = uniqueKeySnapshot.child("status").getValue().toString();
                            realHomeScore = Integer.parseInt(uniqueKeySnapshot.child("homescore").getValue().toString());
                            realAwayScore = Integer.parseInt(uniqueKeySnapshot.child("awayscore").getValue().toString());

                            //Loop through the matches of each user
                            try {
                                String userstatus = usersKeySnapshot.child("matches").child(uniqueKeySnapshot.getKey()).child("status").getValue().toString();

                                if(userstatus.equals("SCHEDULED")){
                                    groupRef.child(groupId).child("users").child(usersKeySnapshot.getKey()).child("matches").child(uniqueKeySnapshot.getKey()).child("status").setValue(status);
                                }
                                if (userstatus.equals("FINISHED")) {
                                    homeScore = Integer.parseInt(usersKeySnapshot.child("matches").child(uniqueKeySnapshot.getKey()).child("homescore").getValue().toString());
                                    awayScore = Integer.parseInt(usersKeySnapshot.child("matches").child(uniqueKeySnapshot.getKey()).child("awayscore").getValue().toString());
                                }
                                else{
                                    homeScore = -1;
                                    awayScore = -1;
                                }
                            } catch (Exception e) {
                                homeScore = -1;
                                awayScore = -1;
                            }
                            calc = new CalculateScores();
                            userScore = userScore + calc.calculateWinner(homeScore, awayScore, winnerRule, realHomeScore, realAwayScore);
                            userScore = userScore + calc.calculateTeam(homeScore, awayScore, teamRule, realHomeScore, realAwayScore);
                            userScore = userScore + calc.calculateTotal(homeScore, awayScore, totalRule, realHomeScore, realAwayScore);
                    }


                    users.add(new User(userId,userName,"", Integer.toString(userScore)));
                    Collections.sort(users, new Comparator<User>() {
                        public int compare(User p1, User p2) {
                            return Integer.valueOf(Integer.parseInt(p2.getScore())).compareTo(Integer.parseInt(p1.getScore()));
                        }
                    });
                    }

                rvUsers.setAdapter(adapter);
                }



            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });


    }




}
