package com.example.betgroup;

import java.text.SimpleDateFormat;

/**
 * DataModel For Match
 */
public class Match {

    public String home,away,startTime, id;
    public int homescore,awayscore;

    public Match(String matchId, String home, String away, int homescore, int awayscore ,String startTime) {
        this.home = home;
        this.away = away;
        this.homescore = homescore;
        this.awayscore = awayscore;
        this.startTime = startTime;
        this.id = matchId;
    };


    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getAway() {
        return away;
    }

    public void setAway(String away) {
        this.away = away;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getHomescore() {
        return homescore;
    }

    public void setHomescore(int homescore) {
        this.homescore = homescore;
    }

    public int getAwayscore() {
        return awayscore;
    }

    public void setAwayscore(int awayscore) {
        this.awayscore = awayscore;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}



