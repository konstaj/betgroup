package com.example.betgroup;

import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.arch.persistence.room.Dao;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface DaoIF {


        @Query("SELECT * FROM DbUser")
        List<DbUser> getAll();

        @Query("DELETE FROM dbUser")
        void deleteAll();

        @Insert
        void insert(DbUser userdata);

        @Delete
        void delete(DbUser userdata);

        @Update
        void update(DbUser userdata);

    }

