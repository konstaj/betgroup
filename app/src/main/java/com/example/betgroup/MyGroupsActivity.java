package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;

public class MyGroupsActivity extends AppCompatActivity {
    Boolean isnew = false;
    String selectedCompCode, groupId, newgroupId,compName,groupOwnerId,groupOwnerName,groupName;
    ListView groupList;
    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    startActivity(new Intent(MyGroupsActivity.this,ProfileActivity.class));
                    finish();
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                case R.id.navigation_competitions:
                    startActivity(new Intent(MyGroupsActivity.this,GameSearchActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                case R.id.navigation_main:
                    startActivity(new Intent(MyGroupsActivity.this,MainActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_groups);
        groupList = findViewById(R.id.groups_list);
        Intent intent = getIntent();
        intent.getStringExtra("new");
        newgroupId = intent.getStringExtra("groupid");
        selectedCompCode = intent.getStringExtra("competitioncode");
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.getMenu().getItem(3).setChecked(true);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


    }

    public void showGroup(View view){
        TextView groupname = findViewById(R.id.detailname);
        Intent intent = new Intent(MyGroupsActivity.this,SelectedGroupActivity.class);
        intent.putExtra("groupname", groupname.getText()) ;
        intent.putExtra("groupid", groupId);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
    }
    public void setGroupId(String groupId){
        this.groupId = groupId;
    }

}


