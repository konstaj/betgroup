package com.example.betgroup;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Adapter to populate listview for Match searching activity
 */
public class CompetitionAdapter extends ArrayAdapter<Competition> implements View.OnClickListener{

    Context mContext;
    private ArrayList<Competition> competitions;

    public CompetitionAdapter( Context context,  ArrayList<Competition> list) {
        super(context, 0 , list);
        mContext = context;
        competitions = list;
    }

    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.competitionrow,parent,false);

        Competition competition = competitions.get(position);


        TextView name =  listItem.findViewById(R.id.competition_name);
        name.setText( competition.getName());

        TextView area = listItem.findViewById(R.id.competition_country);
        area.setText( competition.getArea());
        TextView startdate = listItem.findViewById(R.id.competition_startdate);
        startdate.setText("Startdate: " +  competition.getStartDate());
        return listItem;
    }

    @Override
    public void onClick(View v) {

    }

}