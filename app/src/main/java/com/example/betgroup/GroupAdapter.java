package com.example.betgroup;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.firebase.ui.auth.data.model.Resource;

import java.util.ArrayList;

/**
 * Adapter to populate listview to reveal available groups
 */
public class GroupAdapter  extends ArrayAdapter<Group> implements View.OnClickListener{

    Context mContext;
    private ArrayList<Group> groups;

    public GroupAdapter( Context context,  ArrayList<Group> list) {
        super(context, 0 , list);
        mContext = context;
        groups = list;
    }

    @Override
    public View getView(int position,  View convertView, ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.grouprow,parent,false);

        Group selected = groups.get(position);


        TextView name =  listItem.findViewById(R.id.group_name);
        name.setText( selected.getName());

        TextView owner = listItem.findViewById(R.id.group_owner);
        owner.setText(getContext().getString(R.string.owner) + ": " + selected.getOwnerName());
        TextView usercount = listItem.findViewById(R.id.group_usercount);
        usercount.setText("Players: " +  Integer.toString(selected.getUserCount()));
        return listItem;
    }

    @Override
    public void onClick(View v) {

    }


}