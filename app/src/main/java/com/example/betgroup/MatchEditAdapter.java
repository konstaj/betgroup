package com.example.betgroup;

import android.content.Context;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

/**
 * Adapter to list the finsihed and scheduled games to be edited.
 */
public class MatchEditAdapter extends ArrayAdapter<Match> implements View.OnClickListener{

        Context mContext;
        int layout,position;
        private ArrayList<Match> matches;


        public MatchEditAdapter( Context context,  ArrayList<Match> list, int layout) {
            super(context, 0 , list);
            mContext = context;
            this.layout = layout;
            matches = list;
        }

        @Override
        public View getView(int position,  View convertView, ViewGroup parent) {
            this.position = position;
            View listItem = convertView;

            if(listItem == null)
                listItem = LayoutInflater.from(mContext).inflate(layout,parent,false);


            final Match match = matches.get(this.position);
            String matchDate = match.getStartTime();
            Date date;
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            SimpleDateFormat format = new SimpleDateFormat(" dd.MM.yyyy hh:mm");



            try {
                // Sort in assending order
                Collections.sort(matches, new Comparator<Match>() {
                    String matchDate = match.getStartTime();
                    Date date;
                    SimpleDateFormat format = new SimpleDateFormat(" dd.MM.yyyy hh:mm");
                    public int compare(Match p1, Match p2) {
                        try {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                            return Long.valueOf(formatter.parse(p1.getStartTime().replaceAll("Z$", "+0000")).getTime()).compareTo(formatter.parse(p2.getStartTime().replaceAll("Z$", "+0000")).getTime());
                        } catch (ParseException e) {
                            Log.e("", "ParseException - dateFormat");
                        }
                        return 0;
                    }
                });

                date = formatter.parse(matchDate.replaceAll("Z$", "+0000"));
                matchDate = format.format(date);

            } catch (ParseException e) {
                Log.e("", "ParseException - dateFormat");
            }


            TextView teams =  listItem.findViewById(R.id.matchTeams);
            teams.setText( match.getHome() + " - " + match.getAway());
            TextView homescore = listItem.findViewById(R.id.row_homescore);
            TextView awayscore = listItem.findViewById(R.id.row_awayscore);
            homescore.setText( Integer.toString(match.getHomescore()));
            awayscore.setText( Integer.toString(match.getAwayscore()));
            if(layout == R.layout.editmatchesrow) {
                TextView startdate = listItem.findViewById(R.id.StartTime);
                startdate.setText(matchDate);
            }
            return listItem;
        }

        @Override
        public void onClick(View v) {

        }


    }