package com.example.betgroup;

import android.Manifest;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Locale;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;


/**
 * This is a activity class to manage profile information
 */
public class ProfileActivity extends AppCompatActivity {

    private static final int REQUEST_FINE_LOCATION = 0;
    public FirebaseDatabase database = FirebaseDatabase.getInstance();
    final DatabaseReference nameref = database.getReference("Users");
    ArrayList<String> names = new ArrayList<>();
    Locale myLocale;
    FirebaseAuth auth = FirebaseAuth.getInstance();
    final String username = auth.getCurrentUser().getDisplayName();
    EditText newName;
    TextView namePlease, deleteyes, deleteno, areyousure;
    Button changeName, fin, en;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    return true;
                case R.id.navigation_main:
                    startActivity(new Intent(ProfileActivity.this, MainActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                case R.id.navigation_competitions:
                    startActivity(new Intent(ProfileActivity.this, GameSearchActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
                case R.id.navigation_groups:
                    startActivity(new Intent(ProfileActivity.this, MyGroupsActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
            }
            return false;
        }
    };

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        newName = findViewById(R.id.newname);
        changeName = findViewById(R.id.changenamebtn);
        namePlease = findViewById(R.id.changeplease);
        en = findViewById(R.id.ukbtn);
        fin = findViewById(R.id.finbtn);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.getMenu().getItem(1).setChecked(true);

        navigation.getMenu().getItem(0).setIcon(R.drawable.betgroup);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLocale("en");

            }
        });

        fin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLocale("fi");

            }
        });

        getDatabaseName();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Called when the user taps the Delete profile button
     * Deleting authed profile in response to button
     */
    public void DeleteProfile(View view) {

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // Get auth credentials from the user for re-authentication. The example below shows
        // email and password credentials but there are multiple possible providers,
        // such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = GoogleAuthProvider.getCredential("email@email.com", "password123");

        // Prompt the user to re-provide their sign-in credentials
        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("Users");
                        myRef.child(user.getUid()).setValue(null);
                        user.delete()
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Intent logout = new Intent(ProfileActivity.this, Login.class);
                                            //Closing all activities
                                            logout.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            finishAffinity();
                                            startActivity(logout);

                                        }
                                    }
                                });

                    }
                });
    }

    /**
     * Called when the user taps the Log Out button
     * Log out authed user in response to button
     */
    public void logOut(View view) {

        //LOG OUT
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {

                        // Ask user to log in
                        Intent intent = new Intent(ProfileActivity.this, Login.class);
                        //Closing all activities
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finishAffinity();
                        startActivity(intent);

                    }
                });

        finish();
    }


    /**
     * Method for changing display name
     * validates the name to be unique
     */

    public void nameChange(View view) {

        final String name = newName.getText().toString();
        if (name.equals(username)) {
            namePlease.setVisibility(View.VISIBLE);

            ActivityCompat.requestPermissions(ProfileActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_FINE_LOCATION);
        }

        nameref.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot uniqueKeySnapshot : dataSnapshot.getChildren()) {
                    String databaseName = uniqueKeySnapshot.child("name").getValue().toString();
                    names.add(databaseName);
                }

                if (names.contains(name)) {
                    newName.setText(R.string.namenotavail);

                    Toast.makeText(getApplicationContext(), R.string.namenotavail,
                            Toast.LENGTH_SHORT).show();
                }

                if (name.length() > 15) {
                    newName.setText(R.string.nametoolong);

                    Toast.makeText(getApplicationContext(), R.string.nametoolong,
                            Toast.LENGTH_SHORT).show();
                }

                if (name.length() < 3) {
                    newName.setText(R.string.tooshortname);

                    Toast.makeText(getApplicationContext(), R.string.tooshortname,
                            Toast.LENGTH_SHORT).show();
                }

                if (name.length() < 15 && name.length() > 3 && !names.contains(name)) {
                    nameref.child(auth.getUid()).child("name").setValue(name);

                    namePlease.setVisibility(View.GONE);

                    Toast.makeText(getApplicationContext(), R.string.namechanged,
                            Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                    startActivity(intent);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });


    }

    /**
     * Get the display name from database
     */
    public void getDatabaseName() {
        nameref.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String databasename = dataSnapshot.child(auth.getUid()).child("name").getValue().toString();
                newName.setText(databasename);
                if (databasename.equals("newuser")) {
                    namePlease.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });


    }

    /**
     * this method is popup for confirmation
     *
     * @param view
     */
    public void showAreYouSure(View view) {
        areyousure = findViewById(R.id.areyousure);
        deleteno = findViewById(R.id.deleteno);
        deleteyes = findViewById(R.id.deleteyes);

        areyousure.setVisibility(View.VISIBLE);
        deleteyes.setVisibility(View.VISIBLE);
        deleteno.setVisibility(View.VISIBLE);

    }

    /**
     * this is a setter for system localization
     *
     * @param lang
     */
    public void setLocale(String lang) {
        myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, ProfileActivity.class);
        startActivity(refresh);
    }

    /**
     * this method hides the popup for confirmation
     *
     * @param view
     */
    public void hideAreYouSure(View view) {

        areyousure.setVisibility(View.GONE);
        deleteyes.setVisibility(View.GONE);
        deleteno.setVisibility(View.GONE);
    }


}

