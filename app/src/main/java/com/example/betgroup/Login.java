package com.example.betgroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;





/**
 * This activity asks user to log in
 * with 2 options:
 * 1. Google account
 * 2. Email & password
 * This activity is the activity that starts the app.
 * It is also ran after the user logs out or deletes its profile.
 *
 */
public class Login extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseDatabase database;
    // Choose an arbitrary request code value
    private static final int RC_SIGN_IN = 123;
    // Choose authentication providers
    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build());
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);

         auth = FirebaseAuth.getInstance();

         database = FirebaseDatabase.getInstance();
        if (auth.getCurrentUser() != null) {
            // already signed in
            DatabaseReference myRef = database.getReference("Users");
            startActivity(new Intent(Login.this, MainActivity.class));
            finish();
        } else {
            // not signed in
            // Create and launch sign-in intent
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setTheme(R.style.CustomTheme)
                            .setLogo(R.drawable.betgroup)
                            .setAvailableProviders(providers)
                            .build(),
                    RC_SIGN_IN);

        }
    }
    /**
     * This is the database process that goes trough log in.
     * Checks if user has an account. If not inserts needed info to database.
     * @param requestCode
     * @param resultCode
     * @param data
     */

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // RC_SIGN_IN is the request code you passed into startActivityForResult(...) when starting the sign in flow.
        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            // Successfully signed in
            if (resultCode == RESULT_OK) {
                DatabaseReference MyRef = database.getReference("Users");
                Log.d("ASDF", " " + auth.getCurrentUser().getUid());
                MyRef.child(auth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.hasChild("id")){
                            //IT EXISTS
                            if(dataSnapshot.child("name").getValue().toString().equals("newuser")){

                                startActivity(new Intent(Login.this,ProfileActivity.class));
                                finish();
                            }
                            else{

                                startActivity(new Intent(Login.this,MainActivity.class));
                                finish();

                            }

                        }
                        else{
                            String email = auth.getCurrentUser().getEmail().toString();
                            String name = auth.getCurrentUser().getDisplayName().toString();
                            User user = new User();
                            user.writeNewUser(auth.getUid(), name, email);

                            FirebaseDatabase database = FirebaseDatabase.getInstance();
                            DatabaseReference myRef = database.getReference("Users");
                            myRef.child(auth.getUid()).child("name").setValue("newuser");
                            myRef.child(auth.getUid()).child("groups").child("randomid").setValue("emptygroup");

                            startActivity(new Intent(Login.this,ProfileActivity.class));
                            finish();

                            //IT DOESNT EXISTS
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            } else {
                // Sign in failed
                if (response == null) {
                    // User pressed back button
                    Log.e("Login","Login canceled by User");
                    return;
                }
                if (response.getError().getErrorCode() == ErrorCodes.NO_NETWORK) {
                    Log.e("Login","No Internet Connection");
                    return;
                }
                if (response.getError().getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    Log.e("Login","Unknown Error");
                    return;
                }
            }
            Log.e("Login","Unknown sign in response");
        }
    }


}