package com.example.betgroup;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This Adapter class populates scores in recyclerview from all users in your group
 */
public class ScoreBoardAdapter extends RecyclerView.Adapter<ScoreBoardAdapter.ViewHolder> {
    private ArrayList<User> mUsers;
    private Context mContext;

    public ScoreBoardAdapter(Context mContext, ArrayList<User> users) {

        mUsers = users;
        this.mContext = mContext;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextView, scoreTextView, posTextView;
        public Button messageButton;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameTextView = itemView.findViewById(R.id.sbName);
            scoreTextView = itemView.findViewById(R.id.sbScore);
            posTextView = itemView.findViewById(R.id.positionTextView);
        }
    }



        @Override
        public ScoreBoardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);

            // Inflate the custom layout
            View contactView = inflater.inflate(R.layout.scoreboardrow, parent, false);

            // Return a new holder instance
            ScoreBoardAdapter.ViewHolder viewHolder = new ScoreBoardAdapter.ViewHolder(contactView);
            return viewHolder;
        }

    @Override
    public void onBindViewHolder(@NonNull ScoreBoardAdapter.ViewHolder viewHolder, int position) {



        Log.d("AA", mUsers.toString());
        User user = mUsers.get(position);
        TextView nameTextView= viewHolder.nameTextView;
        nameTextView.setText(user.getName());
        TextView scoreTextview = viewHolder.scoreTextView;
        scoreTextview.setText(user.getScore());
        TextView posTextView = viewHolder.posTextView;
        posTextView.setText(Integer.toString(position + 1));

    }


        // Returns the total count of items in the list
        @Override
        public int getItemCount() {
            return mUsers.size();
        }
    }

