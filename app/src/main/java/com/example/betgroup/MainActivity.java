package com.example.betgroup;

import android.content.Intent;
import android.net.sip.SipSession;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;

/**
 * Landing activity, which observes pending group invitations
 */
public class MainActivity extends AppCompatActivity {
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference userRef = database.getReference("Users");
    private DatabaseReference groupRef = database.getReference("Competitions");
    private ValueEventListener userListener,groupListener;
    private String userId,userName,invitationName,invitationId;
    private TextView clicktojoin, invitationText, invitationgroup;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    startActivity(new Intent(MainActivity.this,ProfileActivity.class));
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
                case R.id.navigation_competitions:
                    startActivity(new Intent(MainActivity.this,GameSearchActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
                case R.id.navigation_groups:
                    startActivity(new Intent(MainActivity.this,MyGroupsActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.getMenu().getItem(0).setChecked(true);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        invitationgroup = findViewById(R.id.grouptojoin);
        invitationText = findViewById(R.id.invitationsmodal);
        clicktojoin = findViewById(R.id.clicktojoin);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {

                    userId = dataSnapshot.child(auth.getUid()).child("id").getValue().toString();
                    userName = dataSnapshot.child(auth.getUid()).child("name").getValue().toString();
                    invitationName = dataSnapshot.child(auth.getUid()).child("invitation").child("name").getValue().toString();
                    invitationId = dataSnapshot.child(auth.getUid()).child("invitation").child("id").getValue().toString();
                    if(invitationId!= null){

                        invitationgroup.setVisibility(View.VISIBLE);
                        clicktojoin.setVisibility(View.VISIBLE);

                        invitationgroup.setText(invitationName);
                        invitationText.setText(R.string.invitationfound);
                    }

                } catch (Exception e) {

                    invitationgroup.setVisibility(View.GONE);
                    clicktojoin.setVisibility(View.GONE);
                    invitationgroup.setText(R.string.noinvitations);
                }

            }
                @Override
                public void onCancelled (DatabaseError databaseError){


            }
        });

    }


    /**
     * send Data on invitation acception
     * @param view
     */
    public void acceptInvitation(View view){
       groupRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try{

                    groupRef.child(invitationId).child("users").child(auth.getUid()).child("name").setValue(userName);
                    for(DataSnapshot uniqueKeySnapshot : dataSnapshot.child(invitationId).child("matches").getChildren()){
                        final String matchId = uniqueKeySnapshot.getKey();
                        final String homename = uniqueKeySnapshot.child("home").getValue().toString();
                        final String awayname = uniqueKeySnapshot.child("away").getValue().toString();
                        final Integer homescore = Integer.parseInt(uniqueKeySnapshot.child("homescore").getValue().toString());
                        final Integer awayscore = Integer.parseInt(uniqueKeySnapshot.child("awayscore").getValue().toString());
                        final String startTime = uniqueKeySnapshot.child("startTime").getValue().toString();
                        final String status = uniqueKeySnapshot.child("status").getValue().toString();

                        if(!status.equals("FINISHED")) {

                                        groupRef.child(invitationId).child("users").child(auth.getUid()).child("matches").child(matchId).child("home").setValue(homename);
                                        groupRef.child(invitationId).child("users").child(auth.getUid()).child("matches").child(matchId).child("away").setValue(awayname);
                                        groupRef.child(invitationId).child("users").child(auth.getUid()).child("matches").child(matchId).child("homescore").setValue(homescore);
                                        groupRef.child(invitationId).child("users").child(auth.getUid()).child("matches").child(matchId).child("awayscore").setValue(awayscore);
                                        groupRef.child(invitationId).child("users").child(auth.getUid()).child("matches").child(matchId).child("startTime").setValue(startTime);
                                        groupRef.child(invitationId).child("users").child(auth.getUid()).child("matches").child(matchId).child("status").setValue(status);

                            }

                    }

                } catch (Exception e) {
        }
                userRef.child(auth.getUid()).child("invitation").removeValue();
                invitationgroup.setVisibility(View.GONE);
                clicktojoin.setVisibility(View.GONE);
                invitationgroup.setText(R.string.noinvitations);
    }
    @Override
    public void onCancelled (DatabaseError databaseError){


    }
});


    }

}
