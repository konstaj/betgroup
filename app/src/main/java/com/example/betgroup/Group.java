package com.example.betgroup;

/**
 * Data-model for Group
 */
public class Group {
    String name, ownerId, ownerName, id ;
    int userCount;
    Integer teamscore, totalscore, winner;


    public Group(String name, String id, String ownerid, String ownerName, int userCount, Integer winner, Integer totalscore, Integer teamscore) {
        this.name = name;
        this.ownerId = ownerid;
        this.ownerName = ownerName;
        this.userCount = userCount;
        this.teamscore = teamscore;
        this.totalscore = totalscore;
        this.winner = winner;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getUserCount() {
        return userCount;
    }

    public void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Integer getTeamscore() {
        return teamscore;
    }

    public void setTeamscore(Integer teamscore) {
        this.teamscore = teamscore;
    }

    public Integer getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(Integer totalscore) {
        this.totalscore = totalscore;
    }

    public Integer getWinner() {
        return winner;
    }

    public void setWinner(Integer winner) {
        this.winner = winner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}