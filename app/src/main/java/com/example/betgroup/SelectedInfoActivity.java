package com.example.betgroup;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import static android.support.design.bottomnavigation.LabelVisibilityMode.LABEL_VISIBILITY_LABELED;

/**
 * This acticity shows rules for the group and lets owner to modify them
 */
public class SelectedInfoActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    List<DbUser> usersList;
    private InviteAdapter adapter;
    User selectedUser;
    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference userRef = database.getReference("Users");
    DatabaseReference groupRef = database.getReference("Competitions");
    ValueEventListener userListener, groupListener;
    ConstraintLayout editrules;
    ListView usersListView;
    TextView moduleTitle,winnernumber,teamscorenumber,totalscorenumber;
    EditText editWinner,editTotalscore,editTeamscore,editName;
    Integer groupWinner,groupTotalscore,groupTeamscore;
    String userId, selectedCompCode, groupId, userEmail,userName,groupOwnerId,groupOwnerName,groupName;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_back:
                    startActivity(new Intent(SelectedInfoActivity.this,MyGroupsActivity.class));
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    finish();
                    return true;
                case R.id.navigation_matches:
                    Intent intent2 = new Intent(SelectedInfoActivity.this,SelectedMatchesActivity.class);
                    intent2.putExtra("groupname",groupName);
                    intent2.putExtra("groupid",groupId);
                    startActivity(intent2);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
                case R.id.navigation_stats:
                    Intent intent3 = new Intent(SelectedInfoActivity.this,SelectedGroupActivity.class);
                    intent3.putExtra("groupname",groupName);
                    intent3.putExtra("groupid",groupId);
                    startActivity(intent3);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectedinfo);

        moduleTitle = findViewById(R.id.moduletext_selectedinfo);
        BottomNavigationView navigation = findViewById(R.id.navigation2);
        navigation.getMenu().getItem(3).setChecked(true);
        navigation.setLabelVisibilityMode(LABEL_VISIBILITY_LABELED);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        moduleTitle.setText(getIntent().getStringExtra("groupname"));
        groupId = getIntent().getStringExtra("groupid");
        usersListView = findViewById(R.id.invitelist);
        totalscorenumber = findViewById(R.id.numbertotalscore);
        winnernumber = findViewById(R.id.number1x2);
        teamscorenumber = findViewById(R.id.numberteamscore);
        editName = findViewById(R.id.editGroupName);
        editWinner = findViewById(R.id.edit1x2);
        editTotalscore = findViewById(R.id.edittotalscore);
        editTeamscore = findViewById(R.id.editTeamscore);
        editrules = findViewById(R.id.editrules);
        userData();

        Log.d("GM", groupId);
        getGroupInfo();


    }
    public void userData(){
        userListener = userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    for (DataSnapshot uniqueKeySnapshot : dataSnapshot.getChildren()) {

                        //Loop 1 to go through all the child nodes of groups

                        userId = uniqueKeySnapshot.child("id").getValue().toString();
                        userName = uniqueKeySnapshot.child("name").getValue().toString();
                        userEmail = uniqueKeySnapshot.child("email").getValue().toString();
                        if (!userId.equals(auth.getUid())) {
                            saveUserToDb(new User(userId, userName, userEmail, ""));
                        }
                    }

                } catch (Exception e) {

                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void clearDB() {
        class ClearDB extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .userDao()
                        .deleteAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        ClearDB clear = new ClearDB();
        clear.execute();

    }

    private void saveUserToDb(final User user) {

        class SaveItem extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                DbUser dbuser = new DbUser();
                dbuser.setUserID(user.getId());
                dbuser.setName(user.getName());
                dbuser.setEmail(user.getEmail());
                dbuser.setScore(user.getScore());

                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .userDao()
                        .insert(dbuser);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                getUsers();
            }
        }

        SaveItem item = new SaveItem();
        item.execute();
    }

    private void getUsers() {
        class GetUsers extends AsyncTask<Void, Void, List<DbUser>> {

            @Override
            protected List<DbUser> doInBackground(Void... voids) {
                List<DbUser> list = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .userDao()
                        .getAll();
                clearDB();
                usersList = list;
                return list;
            }

            @Override
            protected void onPostExecute(List<DbUser> dbusers) {
                super.onPostExecute(dbusers);
                adapter = new InviteAdapter(SelectedInfoActivity.this, usersList);
                usersListView.setAdapter(adapter);

                usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        DbUser selectedUser = usersList.get(position);

                        userRef.child(selectedUser.getUserID()).child("invitation").child("name").setValue(groupName);
                        userRef.child(selectedUser.getUserID()).child("invitation").child("id").setValue(groupId);
                        Toast.makeText(getApplicationContext(), R.string.invited,
                                Toast.LENGTH_SHORT).show();
                        clearDB();
                        // Set text in right Fragment TextView.

                    }
                });
            }
        }

        GetUsers getusers = new GetUsers();
        getusers.execute();

    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {

    }

    public void getGroupInfo () {
        groupListener = groupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                    userId = dataSnapshot.child(groupId).getKey();
                    groupOwnerId = dataSnapshot.child(groupId).child("owner").child("id").getValue().toString();
                    groupOwnerName = dataSnapshot.child(groupId).child("owner").child("name").getValue().toString();
                    groupName = dataSnapshot.child(groupId).child("name").getValue().toString();
                    groupTeamscore = dataSnapshot.child(groupId).child("rules").child("teamscore").getValue(Integer.class);
                    groupTotalscore = dataSnapshot.child(groupId).child("rules").child("totalscore").getValue(Integer.class);
                    groupWinner = dataSnapshot.child(groupId).child("rules").child("1X2").getValue(Integer.class);

                    if(groupOwnerId.equals(auth.getUid())){
                        editrules.setVisibility(View.VISIBLE);
                        editName.setText(groupName);
                        editWinner.setText(groupWinner.toString());
                        editTeamscore.setText(groupTeamscore.toString());
                        editTotalscore.setText(groupTotalscore.toString());
                    }
                    totalscorenumber.setText(groupTotalscore.toString());
                    teamscorenumber.setText(groupTeamscore.toString());
                    winnernumber.setText(groupWinner.toString());




            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    public void SaveRules(View view){

        groupRef.child(groupId).child("name").setValue(editName.getText().toString());
        groupRef.child(groupId).child("rules").child("teamscore").setValue(Integer.parseInt(editTeamscore.getText().toString()));
        groupRef.child(groupId).child("rules").child("totalscore").setValue(Integer.parseInt(editTotalscore.getText().toString()));
        groupRef.child(groupId).child("rules").child("1X2").setValue(Integer.parseInt(editWinner.getText().toString()));
        moduleTitle.setText(editName.getText().toString());

    }
    }




